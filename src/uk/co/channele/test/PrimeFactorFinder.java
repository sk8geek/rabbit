package uk.co.channele.test;

import com.rabbitmq.client.*;
import java.io.IOException;
import java.util.HashSet;

class PrimeFactorFinder {

    Consumer consumer;
    Channel inputChannel;
    Channel outputChannel;

    public static void main(String[] args) throws Exception {

        PrimeFactorFinder finder = new PrimeFactorFinder();
        finder.start(args[0]);

    }

    void start(String queueName) throws Exception {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setUsername("steven");
        factory.setPassword("passwd123");
        factory.setHost("192.168.1.200");
        Connection connection = factory.newConnection();

        inputChannel = connection.createChannel();
        inputChannel.basicQos(1);
        inputChannel.queueDeclare(queueName, true, false, false, null);

        outputChannel = connection.createChannel();
        outputChannel.queueDeclare("response", true, false, false, null);

        System.out.println(" [*] Waiting for messages. To exit press CTRL+C");

        initConsumer();

        inputChannel.basicConsume(queueName, false, consumer);
    }


    void initConsumer() throws Exception {
        consumer = new DefaultConsumer(inputChannel) {

            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body)
                    throws IOException {
                String message = new String(body, "UTF-8");
                System.out.println(" [x] Received '" + message + "'");
                try {
                    String response = findFactors(Long.valueOf(message));
                    outputChannel.basicPublish("", "response", null, response.getBytes("UTF-8"));
                    inputChannel.basicAck(envelope.getDeliveryTag(), true);
                    System.out.println(response);
                } catch (Exception e) {
                    inputChannel.basicAck(envelope.getDeliveryTag(), false);
                }
            }

        };
    }


    String findFactors(Long number) {
        StringBuilder h = new StringBuilder();
        Long test = 2L;
        HashSet<Long> factorSet = new HashSet<Long>();
        while (test < (number / 2)) {
            Double precise = new Double(number.doubleValue() / test.doubleValue());
            if (number / test == precise) {
                factorSet.add(number / test);
                h.append(test + ", ");
                number = (number / test);
                test = 2L;
            } else {
                test++;
            }
        }
        factorSet.add(number);
        h.append(number + ".");
        return h.toString();
    }

}