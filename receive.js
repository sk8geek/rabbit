#!/usr/bin/env node

var amqp = require('amqplib/callback_api');

amqp.connect('amqp://steven:passwd123@192.168.1.200', function(err, conn) {
    conn.createChannel(function(err, ch) {
        var q = 'response';

        ch.assertQueue(q, {durable: true});
        console.log(" [*] Waiting for messages in %s. To exit press CTRL+C", q);
        ch.consume(q, function(msg) {
            try {
                console.log(" [x] Received %s", msg.content.toString());
                ch.ack(msg);
            } catch (e) {
                console.log("something broke");
            }
        },
        {noAck: false});
    });
});

