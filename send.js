#!/usr/bin/env node

var amqp = require('amqplib/callback_api');

amqp.connect('amqp://steven:passwd123@192.168.1.200', function (err, conn) {
    conn.createChannel(function (err, ch) {
        var q = 'alpha';
        var numLen = 8;

        ch.assertQueue(q, {durable: true});
        // Note: on Node 6 Buffer.from(msg) should be used

        for (var i = 0; i < 1000; i++) {

            var number = "";
            for (var j = 0; j < numLen; j++) {
                number = number + parseInt(Math.random() * 10, 10);
            }

            ch.sendToQueue(q, new Buffer(number));
            console.log(" [x] Sent %s", number);

        }
    });
    setTimeout(function () {
        conn.close();
        process.exit(0)
    }, 500);
});
